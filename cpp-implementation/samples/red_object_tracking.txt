This program is compiled by default, and captures video from default camera and detects and tracks red objects.

Keys:

  - Esc or "q": exit.
  - "s": save images of blobs to files named "redobject_blob_#####.png".
