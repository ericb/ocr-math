# cvBlob

This library allows you to find and track blobs on a picture, using OpenCV. Currently, i'm testing OpenCV 3.4.x, but not sure it will work with recent OpenCV version like 4.x.
The reason is that some C functions are used, while they have been removed from OpenCV 4.x.


## Dependencies

In order to compile cvBlob you need the following:

* CMAKE https://cmake.org
* OpenCV  https://opencv.org
* A recent C++ compiler, that supports some C++11 features

## Compiling and installing

### Linux

In Linux, CMake can be used.
Feel free to add meson support

#### CMake

The library is built on top of the sources.
TODO : add "cmake install"

### Windows

Nothing yet // complete me

### Installation path

#### CMake

To change the destination path for the installation, set the `CMAKE_INSTALL_PREFIX` variable:
```shell
cmake . -DCMAKE_INSTALL_PREFIX=<installation_path>
```

### Problems

Encountered an issue? [Tell us about it!](https://framagit.org/ericb/ocr-math/-/issues)

## Information

IMPORTANT :

I'm not the official cvblob maintainer, and the current version is an old one. Please note that the official home page of cvBlob is https://github.com/Steelskin/cvblob

## Author and contributors

Comments, suggestions and, why not, donations to:

* [Eric Bachard](mailto:ericb), current ocr-math maintainer
