#!/bin/bash

# Author : Eric Bachard
# License MIT
# Created : friday 17 déc. 2021 21:27:36 CET

BUILD_DIR=build

# default value, don't forget to change if you need something else
CMAKE_INSTALL_PREFIX=/usr/local

S_UNAME=`uname -s`

if test X${S_UNAME} = "XLinux"
    then
        if ! test -d ${BUILD_DIR}
           then
                mkdir ${BUILD_DIR}
        fi

        pushd .
        cd ${BUILD_DIR}

        # push the current build directory
        # build
        cmake -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX} \
        ..
        make -j4

        # install. Uncomment me if you have admin rights
        # sudo make install
        # sudo ldconfig
        popd
fi

exit


