# OCR-Math

Project cloned from https://github.com/ebachard/OCR-Math, itself cloned from https://github.com/leonsbuddydave/OCR-Math

Just a try, using cmake instead of existing code blocks. 

Current state : builds and works fine, BUT depends on OpenCV 3.4.x, and need to be rewritten partialy.

# Linux build

## Dependencies : the c++ part depends on 

- OpenCV 3.4.0 (because using cSaveImage and some other prehistorical C parts, to be removed soon)
- libboost_filesystem  libboost_filesystem
- cvblob library (included, since Google probably lost the last repo containing the project)

## Howto compile the library + the binary (testing purpose)

````
cd cpp-implementation
./create_build.sh
````
If nothing goes wrong, means all dependencies are installed, everything just works on Linux.


cd in the right directory :

````
cd build

````
To test the example :

````
./OCR-Math

````

Other test (needs a connected webcam, seen as /dev/video0 on Linux)

````
./samples

````

(esc or q to quit)


## To be continued

Eric Bachard  le 21 décembre 2021
